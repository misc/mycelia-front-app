[![build status](https://framagit.org/mycelia/mycelia-front-app/badges/master/build.svg)](https://framagit.org/mycelia/mycelia-front-app/pipelines)
[![coverage report](https://framagit.org/mycelia/mycelia-front-app/badges/master/coverage.svg)](https://mycelia.tools/ci/mycelia-front-app/coverage/)
[![maintainability](https://mycelia.tools/ci/mycelia-front-app/plato/maintainability.svg)](https://mycelia.tools/ci/mycelia-front-app/plato/)
[![License: AGPL v3](https://demo.mycelia.tools/licence-AGPLv3.svg)](http://www.gnu.org/licenses/agpl-3.0)

# Mycelia
Visualisation de graph sociaux

## [Démo en ligne](https://demo.mycelia.tools/)

## Usage basique (sans authentification ni édition des données)

- [Télécharger le projet pré-optimisé](https://framagit.org/mycelia/mycelia-front-app/-/jobs/artifacts/master/download?job=staticPackage)
- Remplacer le contenu du dossier `allData` par vos données et configuration (éditable manuellement par vos soins)
- Héberger le résultat ou vous voulez (ou ouvrir directement `index.html` dans votre navigateur pour consulter en local, même **hors ligne**)

<!--
## Installation complète

prérequis : node.js, git

```sh
npm install
npm start
```

## Hébergement avec docker

```bash
docker-compose up
```

## Structure des données :
Chaque noeud doit avoir un libellé et un type. Il peut contenir d'autres champs optionnels.
```json
{"nodes":[
	{"label":"John Smith","type":"agent"},
	{"label":"M. Dupont","type":"avocat","dateBirth":"1970-01-01","comment":"# M. Dupont\n ## Sa vie\n ## Son oeuvre\ ..."}
]
}
```
Dans la configuration, chaque type devra être associé à un calque ou sous calque pour renseigner la légende et son style d'affichage.
```json
{"nodeLayers":[
	{"id":"human","label":"Personne physique","color":"#AA3388","picto":"staticApp/appImg/people.svg",
	"subLayers":[
		{"id":"agent","label":"Agent","picto":"staticApp/appImg/people.svg"},
		{"id":"juridique","label":"Professionnel de la loi","picto":"staticApp/appImg/people.svg",
		"subLayres":[
			{"id":"avocat","label":"Avocat","picto":"staticApp/appImg/people.svg"}
		]}
	]}
]
}
```
-->
