const execSync = require('child_process').execSync;
const StringDecoder = require('string_decoder').StringDecoder;
const decoder = new StringDecoder('utf8');

const exec = (cmd) => decoder.write(execSync(cmd)) ;

execSync('git checkout master');
let shaList = exec('git rev-list master').split('\n');
shaList.pop();
console.log('commits from project start : ',shaList.length);

const commitList = shaList.map((sha)=> {
	const dateSha = {
		'date': exec('git show -s --format=%ci '+sha),
		'sha':sha
	};
	return dateSha;
});
commitList.sort((a,b)=>{
	if ( a.date < b.date ) return -1;
	if ( a.date > b.date ) return 1;
	return 0;
});
const oneByDayMax = {};
for(let commit of commitList) oneByDayMax[commit.date.substring(0,10)] = commit.sha;
console.log('days with commits, from project start : ', Object.keys(oneByDayMax).length);

for(let date in oneByDayMax){
	const sha = oneByDayMax[date];
	execSync('git checkout '+sha);
	execSync('es6-plato -r -t "Mycelia-front-app Code Source Analysis" -D "'+date+'" -x node_modules -d ../plato/ ./');
}

function badgeColor(percent) {
	let color = 'red';
	if(percent>25) color = 'orange';
	if(percent>50) color = 'yellow';
	if(percent>70) color = 'yellowgreen';
	if(percent>80) color = 'green';
	if(percent>90) color = 'brightgreen';
	return color;
}

const report = require('../../plato/report.json');
const maintainabilityScore = Math.round(parseFloat(report.summary.average.maintainability));
let maintainabilityColor = badgeColor(maintainabilityScore);

const sloc = report.summary.total.sloc;
execSync('wget -q -O ../plato/maintainability.svg https://img.shields.io/badge/maintainability-'+maintainabilityScore+'-'+maintainabilityColor+'.svg');
execSync('wget -q -O ../plato/total-sloc.svg "https://img.shields.io/badge/total sloc-'+sloc+'-blue.svg"');
