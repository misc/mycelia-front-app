const allTestFiles = [];
const TEST_REGEXP = /(spec|test)\.js$/i;

jasmine.DEFAULT_TIMEOUT_INTERVAL = 1000;
function Spy(name) {
	this.spy = jasmine.createSpy(name?name:'dummy');
	return this.spy;
}
// Get a list of all the test files to include
Object.keys(window.__karma__.files).forEach(function (file) {
  if (TEST_REGEXP.test(file)) {
    // Normalize paths to RequireJS module names.
    // If you require sub-dependencies of test files to be loaded as-is (requiring file extension)
    // then do not normalize the paths
    const normalizedTestModule = file.replace(/^\/base\/|\.js$/g, '');
    allTestFiles.push(normalizedTestModule)
  }
});

require.config({
  // Karma serves files under /base, which is the basePath from your config file
  baseUrl: '/base',
	waitSeconds: 100,
	paths: {
		'js-yaml': '/base/node_modules/js-yaml/dist/js-yaml',
		'd3': '/base/node_modules/d3/build/d3',
		'mathjs': '/base/node_modules/mathjs/dist/math',
		'marked': '/base/node_modules/marked/lib/marked'
	},
  // dynamically load all test files
  deps: allTestFiles,

  // we have to kickoff jasmine, as it is asynchronous
  callback: window.__karma__.start
});
