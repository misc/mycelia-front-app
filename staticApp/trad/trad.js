define([
	'./tradChooser',
	'./tradLoader',
	'./tradRenderer',
	'./tradEditor'
], (tChooser, tLoader, tRenderer, tEditor) => {
	function init() {
		tRenderer.init();
		tChooser.init();
		tLoader.init();
		tEditor.init();
	}

	return {
		init,
		"t":tRenderer.t
	};
});
