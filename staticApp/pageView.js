define([
	'./smartEvents',
	'./htmlTools',
	'./trad/trad',
	'marked'
], (ev, htmlTools, trad ,marked) => {
	const buildNode = htmlTools.buildNode;

	let contentArea;
	let config = {};
	function init() {
		listenerInit();
		ev.need('config',(cfg)=>{
			config = cfg;
			contentArea = document.getElementById("details"); //fixme: depuis config
			ev.send('pageView.ready');
		});
	}
	function listenerInit() {
		ev.on('pageView.ready', render);
		ev.on('config.selected change', render);
		ev.on('config.userMode change', render);
	}
	function render(){
		if(config.userMode != "page") return;

		if(!config.selected){
			contentArea.innerHTML = "<div class=header><h2>Tutoriel / Guide</h2></div><section class=wrapper></section><button class=reduce>-</button><button class=expand>+</button>";
			ev.send('pageView.rendered');
			return;
		}

		const header = buildNode(".header."+config.userMode);
		const img = buildNode("img");
		img.src= `allData/img/page/${config.selected}.svg`;
		header.appendChild(img);

		header.appendChild(buildNode("h2",`${config.selected}_title`));

		//TODO: if config.page.pageName.url -> iframe-it or ajax-it
		//sinon config.page.pageName.content (markdown par défaut, html si contentType: html précisé)
		// moteur de rendu isomorphic souhaité.
		const wrapper = buildNode("section.wrapper");

		const litteralContent = recursiveTemplateParser(
			config.page[config.selected].content,
			{config},
			`config.page.${config.selected}`,
			trad.t
		);
		wrapper.innerHTML = marked(litteralContent);//FIXME:check XSS vulnerability

		contentArea.innerHTML="";
		contentArea.appendChild(header);
		contentArea.appendChild(wrapper);
		contentArea.appendChild(buildNode('button.reduce','-'));
		contentArea.appendChild(buildNode('button.expand','+'));
		ev.send('pageView.rendered');
	}
	function recursiveTemplateParser(templateContent,dataRoot,relativePrefix,tradFunc){
		return tradFunc(templateContent).replace(/\$([.a-zA-Z0-9_]+)/g,(m,keyword)=>{
			if(keyword[0]==='.') keyword = relativePrefix+keyword;
			const keyPart = keyword.split('.');
			let content = dataRoot;
			while (keyPart.length) content = content[keyPart.shift()];
			return recursiveTemplateParser(content,dataRoot,relativePrefix,tradFunc);
		});

	}
	return {
		init
	}
});
