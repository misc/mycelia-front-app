define([
	'./ymlTools',
	'./Store',
	'./smartEvents',
	'./MonitoredStruct',
	'./structManipulation'

], (ymlTools, Store, ev, mStruct,struct) => {
	'use strict';
	const on = ev.on, send = ev.send;
	let urlHashStore;
	let config;

	function reset(){
		urlHashStore = false;
		config = false;
	}
	function init() {
		listenerInit();
		ymlTools.loadMerge(['staticApp/appDefault.yml', 'allData/config.yml'], 'config.default');
	}

	function listenerInit() {
		on('config.default', initUrlHashStore,1);
		on('config.pre-ready', proxyfyConfigForGlobalDiffusion,1);
		on('config delete', 'config.change');
		on('config change', storeConfig);
		on('hashchange', reloadConfig);
	}

	function initUrlHashStore(defaultConfig) {
		urlHashStore = new Store.UrlHashStore(defaultConfig);	// init url with default values
		const preConfig = urlHashStore.load();
		send('config.pre-ready', preConfig); // init config from url hash
	}

	function proxyfyConfigForGlobalDiffusion(config) {
		const fullConfig = new mStruct.MonitoredStruct(config, 'config');
		setConfig(fullConfig);
		ev.give("config",getConfig);
	}

	function storeConfig(config) {
		urlHashStore.save(config);
	}
	function apply(baseJson, overwritingJson) { //FIXME: déplacer dans structManipulation avec les test associés
		for (let key in overwritingJson) {
			if (typeof overwritingJson[key] === "object" && typeof baseJson[key] !== 'undefined') {
				baseJson[key] = apply(baseJson[key], overwritingJson[key]);
			}
			else baseJson[key] = overwritingJson[key];
		}
		return baseJson;
	}

	function reloadConfig() {
		const oldConf = struct.clone(config);
		const newConf = urlHashStore.load();
		const diff = struct.diff(oldConf,newConf);
		apply(config,diff);
	}
	function setConfig(fullConfig) {
		config = fullConfig;
	}

	function getConfig() {
		return config;
	}

	return {
		reset,
		init,
		getConfig
	}
});
