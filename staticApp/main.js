requirejs.config({
	waitSeconds: 100,
	paths: {
		'js-yaml': '../node_modules/js-yaml/dist/js-yaml',
		'd3': '../node_modules/d3/build/d3',
		'mathjs': '../node_modules/mathjs/dist/math',
		'marked': '../node_modules/marked/lib/marked'
	}
	// paths: {
	// 	'js-yaml': ['//cdnjs.cloudflare.com/ajax/libs/js-yaml/3.8.1/js-yaml.min','../node_modules/js-yaml/dist/js-yaml'],
	// 	'd3': ['//d3js.org/d3.v4.min','../node_modules/d3/build/d3'],
	// 	'mathjs': ['//cdnjs.cloudflare.com/ajax/libs/mathjs/3.12.2/math.min','../node_modules/mathjs/dist/math']
	// }
});
requirejs([
	"smartEvents",
	'configLoader',
	'./trad/trad',
	"mainToolsViewDemo",
	"advToolsView",
	"./layerEngine/configParser",
	"./layerEngine/legendView",
	"noticeView",
	"pageView",
	'formLoader',
	'graphDataLoader',
	'graph',
	'userInterface',
	'history'
], (ev, cfg,trad, demo, advToolsView,layerConfParser,legendView, noticeView, pageView, formLoader, gData, graph, ui, history) => {
	console.log('chargement des fichiers js terminé');
	window.top.ev = ev; // exposition du module smartEvents pour pouvoir débuger plus facilement.
	ev.need('config',(c)=>console.log("config diffusée : ",c));
	ev.on("lang.change", (lang)=>console.log("langue active : ",lang));
	ev.need('graph.data',(d)=>console.log("données du graph disponnible : ",d));

	layerConfParser.init();
	cfg.init();
	trad.init();
	ev.need('trad',()=>{
		console.log("traduction appliquée");
		demo.init();
		advToolsView.init();
		noticeView.init();
		pageView.init();
		gData.init();
		legendView.init();
		formLoader.init();
		ui.init();
		history.init();
		console.log('initialisation terminé');
	});

	//console.log('config diffusée'); (catch init event mais si chargé trop tard, va demander après coup lors de l'évènement disant que tout est chargé)
	}
);

